# vue-base

> A Vue.js project

## Build Setup

``` bash
# install dependencies
npm install

# serve with hot reload at localhost:8080
npm run dev

# build for production with minification
npm run build

# build for production and view the bundle analyzer report
npm run build --report

# run unit tests
npm run unit

# run e2e tests
npm run e2e

# run all tests
npm test
```

For a detailed explanation on how things work, check out the [guide](http://vuejs-templates.github.io/webpack/) and [docs for vue-loader](http://vuejs.github.io/vue-loader).

Running in production server steps (Linux - Ubuntu 16.04): 
1. Install PM2
2. Run -> npm install
3. Use server.js root file as PM2 application
4. Run -> npm run build
5. Run -> pm2 start <application-number>
6. Run -> pm2 save
7. If you need to stop the application in PM2, run -> pm2 stop <application-number>