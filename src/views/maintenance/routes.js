import Trainings from './trainings/Trainings'
import Users from './users/Users'

export default [
  { name: 'users-default', path: '', redirect: '/maintenance/users' },
  { name: 'users', path: '/maintenance/users', component: Users },
  { name: 'trainings', path: '/maintenance/trainings', component: Trainings }
]
