import PageNotFound from './PageNotFound'
import NoAuthorization from './NoAuthorization'

export default [
  {name: 'NoAuthorization', path: '/error/401', component: NoAuthorization},
  {name: 'PageNotFound', path: '/error/404', component: PageNotFound}
]
