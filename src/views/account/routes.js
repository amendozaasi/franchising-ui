import AccountInformation from './AccountInformation'

export default [
  {name: 'accountInformation', path: '/account', component: AccountInformation}
]
