import mainService from '@/common/utils/service/main.service'
import jwtService from '@/common/utils/service/jwt.service'

export default {
  namespaced: true,

  state: {
    isAuthenticated: !!jwtService.getToken(),
    user: {},
    errors: {}
  },
  getters: {
    isAuthenticated: state => state.isAuthenticated,
    token: state => state.token
  },
  mutations: {
    SET_LOGIN_ERROR (state, errors) {
      state.isAuthenticated = false
      state.errors = errors
    },
    SET_REGISTRATION_ERROR (state, errors) {
      state.errors = errors
    },
    SET_AUTH (state, data) {
      state.isAuthenticated = true
      state.user = data.user
      state.errors = {}
      jwtService.saveToken(data.user.token)
    },
    LOGOUT (state) {
      state.isAuthenticated = false
      state.user = {}
      state.errors = {}
      jwtService.destroyToken()
    }
  },
  actions: {
    registration ({ commit }, data) {
      commit('START_SPIN', null, {root: true})
      return new Promise((resolve, reject) => {
        mainService.post('auth/register', data)
          .then((response) => {
            if (!response.success) {
              commit('SET_REGISTRATION_ERROR', response.errors)
            }
            commit('STOP_SPIN', null, {root: true})
            resolve(response)
          }, error => {
            commit('STOP_SPIN', null, {root: true})
            reject(error)
          })
      })
    },
    login ({ commit }, data) {
      return new Promise((resolve, reject) => {
        commit('START_SPIN', null, {root: true})
        mainService.post('auth/login', data)
          .then((response) => {
            if (!response.success) {
              commit('SET_LOGIN_ERROR', response.errors)
            } else {
              commit('SET_AUTH', response.data)
            }
            commit('STOP_SPIN', null, {root: true})
            resolve(response)
          }, error => {
            commit('STOP_SPIN', null, {root: true})
            reject(error)
          })
      })
    },
    logout ({ commit }) {
      commit('LOGOUT')
    }
  }
}
