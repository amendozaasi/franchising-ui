import Login from './login/Login'
import Register from './register/Register'

export default [
  { name: 'login', path: '/login', component: Login },
  { name: 'register', path: '/register', component: Register }
]
