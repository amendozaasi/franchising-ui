import routes from './routes'
import store from './store'

export default {
  namespaced: true,

  routes,
  store
}
