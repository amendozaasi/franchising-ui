// The Vue build version to load with the `import` command
// (runtime-only or standalone) has been set in webpack.base.conf with an alias.
import Vue from 'vue'
import 'es6-promise/auto'
import Vuex from 'vuex'
import VueRouter from 'vue-router'
import Vuelidate from 'vuelidate'

import Vuetify from 'vuetify'
import 'vuetify/dist/vuetify.min.css'
import 'material-design-icons-iconfont/dist/material-design-icons.css'

import './styles/main.scss'

import { sync } from 'vuex-router-sync'

import App from './App'
import appModule from './app-module'

import mainService from '@/common/utils/service/main.service'

Vue.config.productionTip = false

Vue.use(VueRouter)
Vue.use(Vuelidate)
Vue.use(Vuetify, {
  theme: {
    primary: '#ee2d24',
    secondary: '#c20905',
    accent: '#9c27b0',
    error: '#9f160d',
    warning: '#ffeb3b',
    info: '#2196f3',
    success: '#4caf50'
  }
})

Vue.use(Vuex)

// Setup
appModule.setupBaseComponents()
mainService.init()

// Routing
const myRouter = new VueRouter({
  routes: appModule.routes
})

// Stores
const myStore = new Vuex.Store({
  modules: appModule.stores
})

appModule.navGuard(myStore, myRouter)

sync(myStore, myRouter)

/* eslint-disable no-new */
new Vue({
  el: '#app',
  router: myRouter,
  store: myStore,
  components: { App },
  template: '<App/>'
})
