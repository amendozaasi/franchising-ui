export default {
  data () {
    return {
      alertMixin_isActive: false,
      alertMixin_type: 'success',
      alertMixin_message: 'Successfully Saved'
    }
  },
  methods: {
    $_alertMixin_open (message, type = 'success') {
      this.alertMixin_type = type
      this.alertMixin_message = message
      this.alertMixin_isActive = true
    },
    $_alertMixin_close () {
      this.alertMixin_isActive = false
    }
  },
  computed: {
    $_alertMixin_getIsActive () {
      return this.alertMixin_isActive
    },
    $_alertMixin_getType () {
      return this.alertMixin_type
    },
    $_alertMixin_getMessage () {
      return this.alertMixin_message
    }
  }
}
