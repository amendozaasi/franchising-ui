export default {
  state: {
    errors: {},
    spinner: false
  },
  getters: {

  },
  mutations: {
    SET_ERROR (state, errors) {
      state.errors = errors
    },
    START_SPIN (state) {
      state.spinner = true
    },
    STOP_SPIN (state) {
      state.spinner = false
    }
  }
}
