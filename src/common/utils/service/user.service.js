import { ID_USER_KEY } from '@/common/config'

export default {
  getUser () {
    return window.localStorage.getItem(ID_USER_KEY)
  },

  saveUser (user) {
    window.localStorage.setItem(ID_USER_KEY, user)
  },

  destroyUser () {
    window.localStorage.removeItem(ID_USER_KEY)
  }
}
