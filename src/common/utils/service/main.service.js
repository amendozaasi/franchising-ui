import axios from 'axios'
import jwtService from './jwt.service'
import {
  API_URL,
  API_KEY
} from '@/common/config'

export default {
  init () {
    axios.defaults.baseURL = API_URL
    axios.interceptors.request.use(function (config) {
      const token = jwtService.getToken() || ''

      config.headers = {'X-Api-Key': API_KEY}

      if (token) {
        config.headers.Authorization = `Bearer ${token}`
      }
      return config
    })
    axios.interceptors.response.use(function (response) {
      if (response.status === 200) {
        // interceptor response
        // if 200 on
        // but errorcode = true
        //   redirect to error page
        //   set error msg(popup)
      }
      return response
    }, function (error) {
      switch (error.response.status) {
        case 401: window.location = '/error/401'
          break
        case 404: window.location = '/error/404'
          break
          // add other errors here
        default:
            // default error view
      }
      return Promise.reject(error.response)
    })
  },
  get (url) {
    return new Promise((resolve) => {
      axios.get(url)
        .then((response) => {
          resolve(response.data)
        })
    })
  },
  post (url, data) {
    return new Promise((resolve) => {
      axios.post(url
        , data
        , {
          headers: {
            'Content-type': 'application/json'
          }
        }
      )
        .then((response) => {
          resolve(response.data)
        })
    })
  },
  put (url, data) {
    return new Promise((resolve) => {
      axios.put(`${url}/${data.id}`,
        data,
        {
          headers: {
            'Content-type': 'application/json'
          }
        }
      )
        .then((response) => {
          resolve(response.data)
        })
    })
  },
  delete (url) {
    return new Promise((resolve) => {
      axios.delete(url)
        .then((response) => {
          resolve(response.data)
        })
    })
  }
}
