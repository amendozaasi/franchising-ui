export default {
  fnameReq: 'First name is required.',
  fnameMax: 'First name must be at most 10 characters long',
  fnameMin: '',

  lnameReq: 'Last name is required.',
  lnameMax: 'Last name must be at most 10 characters long',
  lnameMin: '',

  emailReq: 'Email is required.',
  emailMax: 'E-mail must be at most 20 characters long',
  emailMin: '',
  emailValid: 'Must be valid e-mail',

  passReq: 'Password is required.',
  passMax: 'Password must be at most 15 characters long',
  passMin: '',
  passValid: '',

  cpassReq: 'Confirm password is required.',
  cpassNotSame: 'Confirm password must match password.',
  cpassMax: '',
  cpassMin: '',
  cpassValid: '',

  termsReq: 'You must agree to continue.',

  successRegistered: 'Successfully Registered'
}
