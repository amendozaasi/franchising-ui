export default {
  emailReq: 'Email is required',
  passReq: 'Password is required',
  emailFormat: 'Must be in email address format'
}
