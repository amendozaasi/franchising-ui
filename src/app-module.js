import Vue from 'vue'

import upperFirst from 'lodash/upperFirst'
import camelCase from 'lodash/camelCase'

import DefaultLayout from '@/common/layout/DefaultLayout'
import SidebarLayout from '@/common/layout/SidebarLayout'

import common from './common/index'
import auth from './views/auth/index'
import error from './views/error/index'
import maintenance from './views/maintenance/index'
import account from './views/account/index'

const modules = {
  common,
  auth
}
const mainModules = {
  maintenance,
  account
}
const errorModules = {
  error
}

const routes = [
  {
    path: '/auth',
    component: DefaultLayout,
    children: Object.keys(modules)
      .filter(key => !!modules[key].routes)
      .map(key => modules[key].routes)
      .reduce((a, b) => a.concat(b), [])
  },
  {
    path: '/',
    component: SidebarLayout,
    children: Object.keys(mainModules)
      .filter(key => !!mainModules[key].routes)
      .map(key => mainModules[key].routes)
      .reduce((a, b) => a.concat(b), []),
    meta: { requiresAuth: true }
  },
  {
    path: '*',
    component: DefaultLayout,
    children: Object.keys(errorModules)
      .filter(key => !!errorModules[key].routes)
      .map(key => errorModules[key].routes)
      .reduce((a, b) => a.concat(b), [])
  }
]

const buildStores = () => {
  const output = {}
  Object.keys(modules)
    .filter(key => !!modules[key].store)
    .forEach((key) => {
      output[key] = modules[key].store
    })

  Object.keys(mainModules)
    .filter(key => !!mainModules[key].store)
    .forEach((key) => {
      output[key] = mainModules[key].store
    })

  return output
}

function setupBaseComponents () {
  const requireComponent = require.context(
    './common/components',
    false,
    /Base[A-Z]\w+\.(vue|js)$/
  )
  requireComponent.keys().forEach(fileName => {
    const componentConfig = requireComponent(fileName)
    const componentName = upperFirst(
      camelCase(
        fileName.replace(/^\.\/(.*)\.\w+$/, '$1')
      )
    )
    Vue.component(
      componentName,
      componentConfig.default || componentConfig
    )
  })
}

function navGuard (store, router) {
  router.beforeEach((to, from, next) => {
    const requiresAuth = to.matched.some(record => record.meta.requiresAuth)
    const isAuthenticated = store.getters['auth/isAuthenticated']

    if (isAuthenticated && (to.name === 'login' || to.name === 'register')) {
      next('/')
    } else {
      if (requiresAuth && !isAuthenticated) {
        next('/login')
      } else if (requiresAuth && isAuthenticated) {
        next()
      } else {
        next()
      }
    }
  })
}

export default {
  setupBaseComponents,
  routes,
  stores: buildStores(),
  navGuard
}
